import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, StatusBar, ActivityIndicator, FlatList } from "react-native";

import { useNavigation } from '@react-navigation/native';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  
} from "react-native-chart-kit";

import { Dimensions } from "react-native";
const screenWidth = Dimensions.get("window").width;


const chartConfig = {
  backgroundGradientFrom: "#1E2923",
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "#08130D",
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false // optional
};

function HomeScreen() {
  const navigation = useNavigation();

  const listausuarios = () => {
    navigation.navigate("ListaMedidas");
  }

  return (
    <View style={styles.container}>
    <View style = {styles.encabezado}>
      <View style = {styles.start}>
      <Image
              style={{ 
                width: '109.19pt',
                height: '51.28pt',
                marginTop: '60pt',
                marginLeft: '36pt', 
              }}
              source={require("../assets/img/marca.png")}
            />
      </View>

      <View style = {styles.end}>

      <Image
              style={{ 
                width: '80pt',
                height: '80pt',
                marginTop: '46pt',
                marginRight: '29pt'
                
              }}
              source={require("../assets/img/perfil.png")}
            />

      </View> 
    </View>

    <View style = {styles.body}>
      <Text style={styles.titulo}>Bienvenido</Text>
      <Text style={styles.nombreUsuario}>Juan Aguilar</Text>
      <Text style={styles.estadoUsuario}>Estado: Activo</Text>
      <Text style={styles.numeroSesion}>27</Text>
      <Text style={styles.sesiones}>Sesiones Restantes</Text>
      <Text style={styles.fecha}>Hasta 22/12/2021</Text>
    </View>

    <View style ={styles.medidor}>
      <Text style={styles.rendimiento}>Rendimiento de medidas</Text>

      <TouchableOpacity
        onPress={listausuarios}>
      <Text style={styles.ver}>Ver todo</Text>
      </TouchableOpacity>

    </View>

    <View>
    <View>
      <BarChart
        data={{
          labels: ["17/Ene", "23/Feb", "20/Mar", "10/Abr", "15/May", "18/Jun"],
          datasets: [
            {
              data: [
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100
              ]
            }
          ]
        }}
        width={Dimensions.get("window").width} // from react-native
        height={197}
        yAxisSuffix="kg"
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          //marginLeft: '28pt'
          backgroundColor: "#262323",
          backgroundGradientFrom: "#262323",
          backgroundGradientTo: "#262323",
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 15
            
          },
          propsForDots: {
            r: "6",
            strokeWidth: "2",
            stroke: "#ffa726"
          }
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 15,
          marginLeft: '28pt'
        }}
      />
    </View>
</View> 
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1C1B1B',
    
  },

  encabezado: {
    display: 'grid',
    gridTemplateColumns: '50% 50%' ,
    
  },

  body: {
      display: 'grid',
      gridTemplateColumns: '100%' 
  },
  
  titulo: {
    color: '#fff',
    fontSize: '39pt',
    marginLeft: '36pt'
    
    //fontFamily: 'Dosis-Regular'
  },
  nombreUsuario: {
    color: '#FFF843',
    fontSize: '30pt',
    marginLeft: '36pt',
    //fontFamily: 'Dosis-Bold'
  },
  estadoUsuario:{
    color: '#FFF843',
    fontSize: '18pt',
    marginLeft: '36pt',
    //fontFamily: 'Dosis-Light'  
  },

  numeroSesion:{
    color: '#FFF843',
    fontSize: '141pt',
    //fontFamily: 'Dosis-SemiBold',
    textAlign: "center",
  },

  sesiones: {
    color: '#fff',
    fontSize: '18pt',
    //fontFamily: 'Dosis-Mediaum',
    textAlign: "center",
  },

  fecha: {
    color: '#FFF843',
    fontSize: '18pt',
    fontFamily: 'Dosis-Light',
    textAlign: "center",
  },

  medidor: {
    //width: '375pt',
    display: 'grid',
    marginTop: '38pt',
    marginBottom: '9pt',
    gridTemplateColumns: '50% 50%'

  },

  rendimiento: {
    color: '#fff',
    fontSize: '8pt',
    //fontFamily: 'Dosis-Regular',
    textAlign: "left",
    marginLeft: '28pt'
  },

  ver: {
    color: '#FFF843',
    fontSize: '8pt',
    //fontFamily: 'Dosis-Regular',
    textAlign: "right",
    marginRight: '17pt'
  },

  start: {
    textAlign: 'left'
  },

  end: {
    alignItems: 'flex-end'
  }


  
});


export default HomeScreen;