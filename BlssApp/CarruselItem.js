import React, { useState } from 'react';
import {  View,Text, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar, ActivityIndicator, FlatList, Dimensions } from "react-native";

const {width, height} = Dimensions.get('window')

 const CarruselItem = ({item}) => {
    return (
        <View style = {styles.cardView}>
            <Image style = {styles.image} source = {item.url}/>
                <View style = {styles.textView}>
                    <Text style = {styles.itemTitle}>{item.title}</Text>
                </View>

        </View>
    )
    
 }

 const styles = StyleSheet.create ({
    cardView: {
        flex: 1,
        width: 375,
        height: 375,
        backgroundColor: '#FFFFFF',
        //margin:10,
        //shadowColor: '#000',
        //shadowOffset: {width: 0.1, height: 0.1},
        //elevation: 5,
    },

    textView : {
        position: 'absolute',
        bottom: 10,
        margin: 10,
        left:5,

    },

    image: {
        width: 375,
        height: 375,
    },

    itemTitle: {
        fontSize: 30,
        //shadowColor: '#000',
        //shadowOffset: {width: 0.8, height: 0.8},
        //elevation: 5,
        color: '#0435F0',
        fontFamily: 'Prompt',
        fontWeight: 600
        
    },
 })

 export default CarruselItem;