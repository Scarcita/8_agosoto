import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, TextInput, SafeAreaView, TouchableOpacity,FlatList, Dimensions } from "react-native";
import ListaChats from './ListaChats';
import ListaActivos from './ListaActivos';


const Chats = ({navigation}) => {


    return (

        <View style= {styles.container}>
            <View style = {styles.header}>
            <Text style = {styles.titulo}>Chats</Text>
                <Image
                style={{ 
                    width: 24,
                    height: 24,
                    marginTop: 21,
                    marginRight: 24,
                    alignSelf: 'end',
                    
                }}
                source={require("../../assets/image/buscador.png")}
                />
            </View>

            <View style={styles.perfiles}>
                <Text style={styles.activos}>Activos</Text>
                <ListaActivos />
            </View>

            <View style={styles.perfilUsuarios}>
                <Text style={styles.recientes}>Recientes</Text>
                <ListaChats />
            </View>

              
            </View>
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      header: {
        flexDirection: 'row',
		justifyContent: 'space-between',
      },

      titulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 30,
        marginLeft: 70,
      },

      perfiles: {
        marginLeft: 35,
      },

      activos: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 30,
       
      },

      perfilUsuarios: {
        marginLeft: 35,
      },

      recientes: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 22,
      }

    
    })
    


 export default Chats;