import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, TextInput, SafeAreaView, TouchableOpacity,FlatList, Dimensions } from "react-native";



const Successful = ({navigation}) => {


    return (

        <View style= {styles.container}>
            <View style={{backgroundColor: '#fff', height: 90}}>
                <Text style = {styles.titulo}>Checkout</Text>
            </View>
            
            <Image
              style={{ 
                width: 294,
                height: 253,
                alignSelf: 'center',
                //marginLeft: 40,
                //marginRight: 40,
                marginTop: 176,
                marginBottom: 22
                
                
              }}
              source={require("../../assets/image/Successful.png")}
            />

            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("MetodoPago")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                //marginTop: '10%',
                width: 299,
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt',
                    fontWeight: 600
                }}
            >
                Ok
            </Text>
        </TouchableOpacity>

        </View>

        
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#D9D9D9',   
      },

      titulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 44,
        marginLeft: 70,
        marginBottom: 29,
       
      },
      subTitulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 20,
        color: '#000000',
        textAlign: "center",
        marginTop: 39,
        marginBottom: 25
        
      },

       perfiles: {
        marginLeft: 26,
        marginTop: 6,
        display: 'grid',
        gridTemplateColumns: '60% 40%', 
      },

      donacion: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 18,
        color: '#0435F0'
      },
      
      input: {
        width: 316,
        height: 44,
        //margin: 12,
        borderWidth: 1,
        borderColor: '#0435F0',
        borderRadius: 6,
        marginLeft: 50,
        marginRight: 31,
        marginTop: 25,
        color: '#fff',
        fontFamily: 'Prompt',
        fontWeight: 400,
        fontSize: 18,
        paddingLeft: 10,
        marginBottom: 28,
        color: '#000000'

      },

      monto: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 24,
        color: '#0435F0',
        textAlign: "center",
         marginTop: 25,
         marginBottom: 25
      }
      
    })
    


 export default Successful;