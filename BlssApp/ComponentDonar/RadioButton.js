import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text, StyleSheet } from 'react-native';

export default class RadioButton extends Component {
	state = {
		value: null,
	};
	render() {
		const { PROP } = this.props;
		const { value } = this.state;
		return (
			<View>
				{PROP.map(res => {
					return (
						<View key={res.key} style={styles.cajas}>
							<Image
								style={{ 
									width: 30,
									height: 29,
									marginTop: 15,
									marginLeft: 15
									
								}}
								source={require("../../assets/image/mastercard.png")}
								/>
								{/* <Image style = {styles.image} source = {res.img}/> */}
							<Text style={styles.radioText}>{res.name}</Text>
							<TouchableOpacity
								style={styles.radioCircle}
								onPress={() => {
									this.setState({
										value: res.key,
									});
								}}>
                                  {value === res.key && <View style={styles.selectedRb} />}
							</TouchableOpacity>
						</View>
					);
				})}
                {/* <Text>{this.state.value} </Text> */}
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
	},

    cajas: {
        width: 283,
        height: 73,
        borderWidth: 1,
        borderColor: '#0435F0',
        borderRadius: 6,
        //marginLeft: 46,
        //marginRight: 46,
        marginBottom: 14,
        flexDirection: 'row',
		justifyContent: 'space-between',
		alignSelf: 'center',

    },

    radioText: {
        fontFamily: 'Prompt',
        fontSize: 15,
        color: '#000000',
        fontWeight: 500,
        marginTop: 27,
    },
	radioCircle: {
		height: 20,
		width: 20,
		borderRadius: 100,
		borderWidth: 1,
		borderColor: '#0435F0',
		alignItems: 'center',
		justifyContent: 'center',
        marginTop: 27,
        marginRight: 27,
		

	},
	selectedRb: {
		width: 12,
		height: 12,
		borderRadius: 50,
		backgroundColor: '#0435F0',
    },
    result: {
        marginTop: 20,
        color: 'white',
        fontWeight: '600',
        backgroundColor: '#0435F0',
    },
});