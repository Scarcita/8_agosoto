import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, TextInput, SafeAreaView, TouchableOpacity,FlatList, Dimensions } from "react-native";



const Donacion = ({navigation}) => {

    const [factura, onChangeFactura] = React.useState("");

    return (

        <View style= {styles.container}>
            <Text style = {styles.titulo}>Donaciones</Text>
            <Image
              style={{ 
                width: '100%',
                height: 219,
                marginTop: 29,
                marginRight: 29  
              }}
              source={require("../../assets/image/2.jpg")}
            />

            <Text style = {styles.subTitulo}>Cuanto quiere donar?</Text>

            <TextInput
                style={styles.input}
                onChangeText={onChangeFactura}
                value={factura}
                keyboardType="numeric"
                placeholder='Ingrese el monto'   
            />
            <Text style = {styles.monto}>$100</Text>
            <View style={{ borderBottomColor: '#B8B7B7', borderBottomWidth: 1, width: 324, alignSelf: 'center', }} />
            <Text style = {styles.monto}>$50</Text>
            <View style={{ borderBottomColor: '#B8B7B7', borderBottomWidth: 1, width: 324, alignSelf: 'center', }} />
            <Text style = {styles.monto}>$5</Text>


            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("MetodoPago")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                //marginTop: '10%',
                width: 324,
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt',
                    fontWeight: 600
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>

        </View>

        
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      titulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 56,
        marginLeft: 70,
        marginBottom: 29
      },
      subTitulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 20,
        color: '#000000',
        textAlign: "center",
        marginTop: 39,
        marginBottom: 25
        
      },
      
      input: {
        width: 316,
        height: 100,
        borderWidth: 1,
        borderColor: '#0435F0',
        alignSelf: 'center',
        borderRadius: 6,
        marginTop: 25,
        color: '#fff',
        fontFamily: 'Prompt',
        fontWeight: 400,
        fontSize: 18,
        padding: 10,
        marginBottom: 28,
        color: '#000000',
      },

      monto: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 24,
        color: '#0435F0',
        textAlign: "center",
         marginTop: 25,
         marginBottom: 25
      }
      
    })
    


 export default Donacion;