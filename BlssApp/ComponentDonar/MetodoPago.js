import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import RadioButton from '../ComponentDonar/RadioButton'
import { CheckBox } from 'react-native';

const MetodoPago = ({navigation}) => {

    const [isSelected, setSelection] = useState(false);

    const PROP = [
        {
         key: '1',
         img: "../../assets/image/Vector.png",
         name: 'My Stripe'
        },
     
        {
         key: '2',
         img: "../../assets/image/mastercard.png",
         name: 'MasterCard'
        },
        {
         key: '3',
         img: "../../assets/image/paypal.png",
         name: 'PayPal'
        },
        {
         key: '4',
         img: "../../assets/image/visa.png",
         name: 'Visa'
        },
     
     ]
     

    return (

        <View style= {styles.container}>
            <Text style = {styles.titulo}>Checkout</Text>
            <Text style = {styles.subTitulo}>Metodo de pago</Text>

            <View>
                <RadioButton PROP={PROP}/>
            </View>

            <View style= {styles.checkboxcontainer}>
                <CheckBox
                value = {isSelected}
                onValueChange = {setSelection}
                style= {styles.checkbox}>

                </CheckBox>
                <Text style={styles.descripcion}>Donate is anonymous</Text>

            </View>


            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("Successful")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                width: 324,
                height: 38,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt',
                    fontWeight: 600
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>

        </View>

            
    )
}
            
   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      titulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 56,
        marginLeft: 70,
      },
      subTitulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 20,
        color: '#000000',
        marginLeft: 48,
        marginTop: 29,
        marginBottom: 14
        
      },

      checkboxcontainer: {
        flexDirection: 'row',
      },

      checkbox: {
        width: 19,
        height: 19,
        marginLeft: 50,
        borderRadius: 1,
        borderColor: '#0435F0',
        marginBottom: 177,
      },

      descripcion: {
        fontFamily: 'Prompt',
        fontWeight: 400,
        fontSize: 13,
        color: '#000000',
        marginLeft: 8

      }

      
    })
    


 export default MetodoPago;