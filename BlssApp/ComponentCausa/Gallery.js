import React, { useState, useEffect } from 'react';
import * as ImagePicker from 'expo-image-picker';
import { Button, Image, View, Platform, StyleSheet, TouchableOpacity } from 'react-native';



export default function GalleryComponenet() {
	const [image, setImage] = useState(null);
	
	useEffect(() => {
		(async () => {
		if (Platform.OS !== 'web') {
			const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
			if (status !== 'granted') {
			alert('Sorry, Camera roll permissions are required to make this work!');
			}
		}
		})();
	}, []);
	
	const chooseImg = async () => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			aspect: [4, 3],
			quality: 1,			
			allowsEditing: true,
		});
	
		console.log(result);
	
		if (!result.cancelled) {
		   setImage(result.uri);
		}
	};
	
	return (
		<View style={{ flex: 1, alignSelf: 'center' }}>
            {/* <View  onPress={chooseImg}>
            <Text style={styles.cargarImagen}>+</Text>

            </View>
             */}
             
			<Button title="Agregar foto" onPress={chooseImg} style={{
                backgroundColor: '#000000',
                
            }}/>
            {image && <Image source={{ uri: image }} style={{ width: 342, height: 134 }} />}
		</View>
	);
}

const styles = StyleSheet.create({
    
    cargarImagen: {
        backgroundColor: '#D9D9D9',
      },
})