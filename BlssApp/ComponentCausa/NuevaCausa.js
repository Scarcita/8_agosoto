import React from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import Gallery from './Gallery'

function NuevaCausa() {
    const [descripcion, onChangeDescripcion] = React.useState("");
    const [monto, onChangeMonto] = React.useState("");
    const [fecha, onChangeFecha] = React.useState("");
    return (
        <View style={styles.container}>
            <View>
            <Text style = {styles.titulo}>Nueva Causa</Text>
            </View>

            <View>
                <Gallery/>
            </View>

            <View>
                <Text style = {styles.descripcion}>Descripcion de la causa</Text>
            
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeDescripcion}
                    value={descripcion}
                    keyboardType="string"
                    placeholder='Descripcion...'   
                />
            </View>

            
            <View>
                <Text style = {styles.descripcion}>Monto de la donacion</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeMonto}
                    value={monto}
                    keyboardType="numeric"
                    placeholder='0'   
                />
            </View>

            
            <View>
                <Text style = {styles.descripcion}>Fecha de expiracion</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeFecha}
                    value={fecha}
                    keyboardType="numeric"
                    placeholder='15/12/2020'   
                />
            </View>

            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("MetodoPago")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                marginTop: 51,
                width: 324,
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt',
                    fontWeight: 600
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>

        </View>

    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',  
    },

    titulo: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 22,
        color: '#000000',
        marginTop: 30,
        marginLeft: 70,
        marginBottom: 250
      },

    input: {
        width: 340,
        height: 44,
        borderWidth: 1,
        borderColor: '#0435F0',
        alignSelf: 'center',
        borderRadius: 6,
        //marginTop: 25,
        color: '#fff',
        fontFamily: 'Prompt',
        fontWeight: 400,
        fontSize: 18,
        paddingLeft: 10,
        marginBottom: 6,
        color: '#000000',
      },

      descripcion: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 20,
        color: '#000000',
        marginBottom: 3,
        marginLeft: 25
        
      }
})

export default NuevaCausa;

