// import React, { Component } from "react"
// import ReactDOM from "react-dom"
// import { ComposableMap, ZoomableGroup, Geographies, Geography } from "react-simple-maps"

// class Maps extends Component {
//   constructor() {
//     super()

//     this.state = {
//       zoom: 1,
//     }

//     this.handleZoomIn = this.handleZoomIn.bind(this)
//     this.handleZoomOut = this.handleZoomOut.bind(this)
//   }
  
//   handleMoveStart(currentCenter) {
//     console.log("New center: ", currentCenter)
//   }
  
//   handleMoveEnd(newCenter) {
//     console.log("New center: ", newCenter)
//   }

//   render() {
//     return (
//       <div>
//         <button onClick={this.handleZoomIn}>{"Zoom in"}</button>
//         <button onClick={this.handleZoomOut}>{"Zoom out"}</button>
//         <hr />
//         <ComposableMap>
//           <ZoomableGroup 
//           onMoveStart={this.handleMoveStart}
//           onMoveEnd={this.handleMoveEnd}>
//             <Geographies geography={"/path/to/your/topojson-map-file.json or geography object"}>
//               {(geographies, projection) =>
//                 geographies.map(geography => (
//                   <Geography key={geography.id} geography={geography} projection={projection} />
//                 ))
//               }
//             </Geographies>
//           </ZoomableGroup>
//         </ComposableMap>
//       </div>
//     )
//   }
// }

// document.addEventListener("DOMContentLoaded", () => {
//   ReactDOM.render(<Maps />, document.getElementById("app"))
// })
