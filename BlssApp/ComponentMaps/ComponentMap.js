import React from 'react';
import MapView, { Marker, ProviderPropType} from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity} from 'react-native';


export default function ComponentMap() {
    return (
        <View style= {styles.container}>
            <MapView
            style={styles.map}
            initialRegion={{
                latitude: 13.406,
                longitude: 123.3753,
                latitudeDelta: 0.005,
                longitudeDelta: 0.0005
            }}
                showsUserLocation={true}>

                <Marker coordinate={{latitude: 13.406, longitude: 123.3753}}>

                </Marker>

            </MapView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    }
})

// const { width, height} = Dimensions.get('window')
// const ASPECT_RATIO = width / height;
// const LATITUDE = 6.2257614;
// const LONGITUDE = -75.5987337;
// const LONGITUDE_DELTA = 0.0922;
// const LATITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// class Mapa extends React.Component {
//     constructor(props){
//         super(props);

//         this.state = {
//             region: {
//                 latitude: LATITUDE,
//                 longitude: LONGITUDE,
//                 latitudeDelta: LATITUDE_DELTA,
//                 longitudeDelta: LONGITUDE_DELTA
//             }
//         }
//     }

//     render () {
//         return (
//         <View style={styles.container}>
//             <MapView
//             provider= {this.props.provider}
//             style = {styles.map}
//             scrollEnabled = {true}
//             zoomEnabled ={true}
//             pitchEnabled={true}
//             rotateEnabled={true}
//             initialRegion={this.state.region}
//             showsUserLocation={true}
//             followsUserLocation={true}
//             />

//         </View>
//         );
//     }
// }
// Mapa.propTypes = {
//     provider: ProviderPropType,
// }
// const style = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//         alignItems: 'center',
//         justifyContent: 'center'
//     },
//     map: {
//         height: '100%',
//         width: '100%'
//     }
// })
// export default Mapa;