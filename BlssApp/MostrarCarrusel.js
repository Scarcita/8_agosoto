import React, { useState } from 'react';
import {  View,Text, StyleSheet, SafeAreaView, TouchableOpacity,FlatList, Dimensions } from "react-native";
import Slider from '@react-native-community/slider';
import Carrusel from '../BlssApp/Carrusel'
import imageData from './Data/Data'
import ListaUsuarios from './ListaUsuarios';

import * as Progress from 'react-native-progress'
import SingleUser from './SingleUser';


const MostrarCarrusel = ({navigation}) => {
    const[range, setRange] = useState ('10')
    const [Sliding, setSliding] = useState("100")
    
    return (
        <View style={styles.container}>
            <Carrusel data = {imageData}/>
            <View>
                <Text style={{marginLeft: 26, marginRight: 26, fontSize: 12, fontFamily: 'Prompt', fontWeight: 300, marginBottom: 5,}}>Mostramos el diseno que tenemos del porcentaje completado de la causa, una foto destacada y la descripcion de la misma</Text>
                <View style={styles.infMonto}>
                    <Text style={{fontSize:14, color: "#0435F0", marginLeft: 26, fontFamily: 'Prompt',
                            fontWeight: 500}}>${range} raised from $2000</Text>
                    <Text style={{fontSize:14, color: "#0435F0", marginLeft: 26, fontFamily: 'Prompt',
                            fontWeight: 300, textAlign: 'end', marginRight: 26 }}>17 Dias left</Text>
                </View>

                <SafeAreaView>

                    <View style={{marginLeft: 26, marginRight: 26}}>
                        <Progress.Bar progress={0.5} width= {360} height={6} backgroundColor='#0435F0' color='#0435F0' ></Progress.Bar>
                    </View>

                </SafeAreaView>         
        {/* <Slider 
            style={{
                width: '93%',
                height: 6,
                color: "#0435F0",
                marginLeft: 26,
                marginRight: 26
            }}
            minimunValue={10}
            maximunValue={100}
            minimunTrackTintColor="#0435F0"
            maximunTrackTintColor="#000"
            thumbTintColor='#0435F0'
            value={100}
            onValueChange={value => setRange(value)}
            onSlidingStart={() => setSliding('Sliding')}
            onSlidingComplete={() => setSliding('Inactive')}
        /> */}
        </View>
        <View style={styles.perfiles}>
            <ListaUsuarios />
            <Text style={styles.donacion}>from $3456.08</Text>
        </View>
        
        <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("Donacion")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                //marginTop: '10%',
                width: '90%',
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt',
                    fontWeight: 600
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>
        
        <View style={styles.donantes}>
            <SingleUser />
        </View>
        </View>
        
        
    )
}

const styles = StyleSheet.create ({
    container: {
      flex: 1,
      backgroundColor: '#fff', 
         
      },

       perfiles: {
        marginLeft: 26,
        marginTop: 6,
        display: 'grid',
        gridTemplateColumns: '60% 40%', 
      },

      donacion: {
        fontFamily: 'Prompt',
        fontWeight: 600,
        fontSize: 18,
        color: '#0435F0'
      },
      
      donantes: {
        
        
      }
    })

 export default MostrarCarrusel;