import React from "react";
import { View,Text, StyleSheet, TouchableOpacity } from "react-native";


const SesionesProps = ({item}) => {

  const {fecha, plan, precio, sesion} = item

  return (
    <View style = {styles.container}>
      <View style={styles.informacion}>
        <View style={styles.start}>
            <Text style={styles.fecha}>{fecha}</Text>
            <Text style={styles.plan}>{plan}</Text>
            <Text style={styles.precio}>{precio}</Text>
        </View>

        <View style={styles.end}>   
            <Text style={styles.text1}>Sesion</Text>
            <Text style={styles.sesion}>{sesion}</Text>
        </View>
     </View>
      
    </View>
  )
   
}

export default SesionesProps;

const styles = StyleSheet.create ({
    container: {
      flex: 1,
      backgroundColor: '#1C1B1B',
      
  
    },
  
    informacion: {
        display: 'grid',
        gridTemplateColumns: '60% 40%' 
      
    },
  

  start: {
    textAlign: 'star',
    //marginTop: '18pt'
  },

  end: {
    textAlign: 'end',
    //marginTop: '18pt'
  },

  fecha: {
    marginLeft: '24pt',
    color: '#FFF843',
    fontSize: '12pt'
  },

  plan: {
    marginLeft: '24pt',
    marginTop: '6pt',
    color: '#FFF843',
    fontSize: '20pt'
  },

  precio: {
    marginLeft: '24pt',
    marginTop: '6pt',
    color: '#fff',
    fontSize: '12pt',
    marginBottom: '27pt'
  },

  text1: {
    color: '#fff',
    marginRight: '31pt',
    marginBottom: '4pt'
  },

  sesion: {
    color: '#000000',
    width: 57,
    height: 57,
    fontSize: '37pt',
    backgroundColor: '#FFF843',
    borderRadius: '11pt',
    marginLeft: '51pt',
    marginTop: '1pt',
    textAlign: 'end',   
   }
  
  
  });