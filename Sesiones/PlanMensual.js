import React from "react";
import { View,Text, StyleSheet, SafeAreaView, FlatList} from "react-native";
import PlanesMensuales from "./PlanesMensuales";

function PlanMensual() {

  const listaPlanes = [
    {
    id: '1',
    plan: 'Plan Mensual',
    sesiones: '30 Sesiones',
    precio: 'Bs 349',
    },

    {
      id: '2',
      plan: 'Plan Mensual',
      sesiones: '30 Sesiones',
      precio: 'Bs 1040',
    },

    {
      id: '3',
      plan: 'Plan Mensual',
      sesiones: '30 Sesiones',
      precio: 'Bs 2080',
    }
]


  return (

    <SafeAreaView style={{
      backgroundColor: '#1C1B1B',
      width: '100%',
      height: '100%'
    }}>
      <View>
      <Text style={styles.titulo}>Sesiones</Text>
      <Text style={styles.descripcion}>Comprar Sesiones</Text>
      </View>
      <FlatList
      data = {listaPlanes}
      keyExtractor = {(item) => item.id}
      renderItem = {({item}) => <PlanesMensuales planItem = {item}/>}
      />
    </SafeAreaView>  
  )
  
}


export default PlanMensual;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#1C1B1B', 
  },

  titulo: {
    color: '#fff',
    fontSize: '51pt',
    marginLeft: '24pt',
    marginTop: '27pt',
    //fontFamily: 'Dosis-Regular'
  },

  descripcion: {
    color: '#FFF843',
    fontSize: '17pt',
    marginLeft: '24pt',
    //fontFamily: 'Dosis-Regular'
  },

  

});