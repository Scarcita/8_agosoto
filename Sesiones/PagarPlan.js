import React from "react";
import { View,Text, StyleSheet, SafeAreaView, TextInput, TouchableOpacity } from "react-native";


function PagarPLan (props) {
  
  const [factura, onChangeFactura] = React.useState("");
  const [nit, onChangeNit] = React.useState("");

  return (
    <View style={styles.container}>

    <View style={styles.informacion}>
        <Text style={styles.text1}>{props.precio} </Text>
        <Text style={styles.text3}>TOTAL</Text>
        <Text style={styles.text2}>{props.sesiones}</Text>
        <Text style={styles.text3}>SESIONES DESPUES DE LA COMPRA</Text>
    </View>

    <SafeAreaView>
     <Text style={styles.text4}>Nombre de la Factura</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeFactura}
        value={factura}
        keyboardType="numeric"
      />

    <Text style={styles.text5}>CI/NIT</Text>

    <TextInput
        style={styles.input}
        onChangeText={onChangeNit}
        value={nit}
        keyboardType="numeric"
        
      />
    </SafeAreaView>

    <View>
    <Text style={styles.tarjeta}>Pagar con tarjeta de credito/Debito</Text>
    </View>

      
    </View>

    
  )
    
};

export default PagarPLan;

const styles = StyleSheet.create ({
    container: {
      flex: 1,
      backgroundColor: '#1C1B1B',
      justifyContent: 'center',
      textAlign: 'center',
      alignItems: 'center',  
    },
  
    text1: {
      color: '#FFFFFF',
      fontSize: '69pt',
      //marginLeft: '9pt',
      marginTop: '17pt',
      
    },
  
    text2: {
      color: '#FFFFFF',
      fontSize: '69pt',
      //marginLeft: '9pt'
      
    },
  
    text3: {
      color: '#FFF843',
      fontSize: '15pt',
      //marginLeft: '9pt'
    
    },

    text4: {
        color: '#FFFFFF',
        fontSize: '14pt',
        textAlign: 'left',
        marginTop: '47pt',
        marginLeft: '40pt',
        marginRight: '40pt'
    },

    text5: {
        color: '#FFFFFF',
        fontSize: '14pt',
        textAlign: 'left',
        marginTop: '14pt',
        marginLeft: '40pt',
        marginRight: '40pt'
    },

    input: {
        width: '295pt',
        height: '29pt',
        //margin: 12,
        borderWidth: '1pt',
        borderColor: '#FFF843',
        borderRadius: '8pt',
        marginLeft: '40pt',
        marginRight: '40pt',
        marginTop: '6pt',
        color: '#fff'
      },

      tarjeta: {
        backgroundColor: '#383838',
        width: '295pt',
        height: '59pt',
        marginTop: '50pt',
        color: '#FFF843',
        borderRadius: '17pt',
        paddingTop: '22pt',
        paddingLeft: '16pt'
      }
  });