import React from "react";
import { View,Text, StyleSheet, SafeAreaView } from "react-native";


const Medidas = ({item}) => {

  const {fecha, supervisor, peso, brazo, pecho, cintura} = item

  return (
    <View style = {styles.container}>
      <Text style = {styles.fecha}>{fecha} Supervisado por: {supervisor}</Text>
      <View style = {styles.informacion}>
        <View style = {styles.start}>
          <Text style = {styles.peso}>{peso}</Text>
        </View>

        <View style = {styles.end}>
          <Text style = {styles.brazo}>Brazo: {brazo}</Text>
          <Text style = {styles.pecho}>Pecho: {pecho}</Text>
          <Text style = {styles.cintura}>Cintura: {cintura}</Text>
        </View>
      </View>
      
    </View>
  )
  
    
}


export default Medidas;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#1C1B1B',
  },
  
  informacion: {
    display: 'grid',
    gridTemplateColumns: '45% 55%' 
      
  },
  
  start: {
    textAlign: 'star',
    //marginTop: '18pt'
  },

  end: {
    textAlign: 'start',
    //marginTop: '18pt'
  },

  fecha: {
    marginLeft: '24pt',
    color: '#FFF843',
    marginTop: '57',
    fontSize: '12pt'
  },

  supervisor: {
    marginLeft: '24pt',
    color: '#FFF843',
    fontSize: '12pt'
  },

  peso: {
    marginLeft: '31pt',
    marginTop: '6pt',
    color: '#FFF843',
    fontSize: '43pt'
  },

  brazo: {
    marginLeft: '22pt',
    marginTop: '6pt',
    color: '#fff',
    fontSize: '14pt',
   
  },

  pecho: {
    marginLeft: '22pt',
    color: '#fff',
    fontSize: '14pt',
   
  },

  cintura: {
    marginLeft: '22pt',
    color: '#fff',
    fontSize: '14pt',
    marginBottom: '19pt'
   
  },
  
  
   });
